import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Highlight } from '../highlight.directive';
 
@Component({
  selector: 'todo-item',
  template: `
    <p highlight [myHighlight]="'#e8e8e8'" >
      <input type="checkbox" 
        (click)="toggleCompleted()" 
        [checked]="data.completed" />
      {{data.name}}
      <ng-content select="actions"></ng-content>
    </p>
  `,
  styles: [
   // 'p{ background:magenta; }'
  ],
  directives: [
    Highlight  
  ]
})
export class TodoItemComponent implements OnInit {

  @Input() data:any = {}

  @Output() completed = new EventEmitter(); 

  @Output() delete = new EventEmitter();

  toggleCompleted(){
    this.data.completed = !this.data.completed
    this.completed.emit(this.data.completed)
  }

  constructor() { }

  ngOnInit() {
  }

}
