import { Component, OnInit } from '@angular/core';
import { TodoItemComponent } from './todo-item.component';

@Component({
  selector: 'todo-app',
  template: `
      <a (click)="tab = ''"> Show All </a>
      <span> | </span>
      <a (click)="tab = 'archived'"> Show Archived </a>
      
      <div [ngSwitch]="tab">
        <div *ngSwitchCase="'archived'">
          <ul *ngIf="archived.length">
            <li *ngFor="let todo of archived">  
              <todo-item [data]="todo" 
                [class.complete]="todo.completed" 
                (completed)="todoToggleCompleted($event)"
                (delete)="revertTodo(todo)"
              >
              <actions>
                <span (click)="deleteTodo(todo)">&laquo;</span>
              </actions>

              </todo-item>
            </li>
          </ul>
        <p *ngIf="!todos.length"> Kliknij &times; by przywrócićr</p>
        </div>
        <div *ngSwitchDefault>
          <ul *ngIf="todos.length">
            <li *ngFor="let todo of todos">  
              <todo-item [data]="todo" 
                [class.complete]="todo.completed" 
                (completed)="todoToggleCompleted($event)"
                (delete)="deleteTodo(todo)"
              >

              <actions>
              <span (click)="deleteTodo(todo)">&times;</span>
              </actions>
              
              </todo-item>
            </li>
          </ul>
          <p *ngIf="!todos.length"> Dodaj zadanie podając nazwę i wciskając Enter</p>
        </div>
      </div>
 
      <input [(ngModel)]="newTodo" 
        (keydown.enter)="addTodo()"
       />
      <button  (click)="addTodo()">Dodaj</button>
  `,
  styles: [],
  directives: [ TodoItemComponent ]
})
export class TodoAppComponent implements OnInit {

  tab = '';

  todos = [
    {
      name: 'Nauczyć się angulara',
      completed: false
    },
    {
      name: 'Dostać masę ofert pracy jako Ng2 dev!',
      completed: false
    },
  ]
  archived = [];

  addTodo(){
    if(!this.newTodo){
      return;
    }
    this.todos.push({
      name: this.newTodo,
      completed: false
    })
    this.newTodo = '';
  }

  deleteTodo(todo){
    let index = this.todos.indexOf(todo);
    let archived = this.todos.splice(index,1)[0]
    console.log(archived)
    this.archived.push(archived)
  }

  newTodo:string = 'Wpisz todo';

  todoToggleCompleted(completed){
    this.allCompleted = completed;
  }

  allCompleted = false;

  constructor() { }

  ngOnInit() {
  }

}
