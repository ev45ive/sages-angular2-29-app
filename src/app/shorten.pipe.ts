import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(value: any, max: any = 30): any {
    return value.length > max? value.substring(0,max) + '...': value;
  }

}
