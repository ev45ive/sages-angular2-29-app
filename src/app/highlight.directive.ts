import { Directive, ElementRef, HostBinding,
  HostListener, Host, Attribute, Input, OnInit, Renderer } from '@angular/core';

@Directive({
  selector: '[highlight]',
  host:{
    '(mouseenter)': 'mouseenter()',
    '(mouseleave)': 'mouseleave()',
    '[style.background]':'color' 
  }
})
export class Highlight implements OnInit {

  @Input('myHighlight') highlightColor;
  //highlightColor;

  //@HostListener('mouseenter')
   mouseenter(){
      //this.el.nativeElement.style.background 
      this.color = this.highlightColor;
  }
  

  //@HostListener('mouseleave')
   mouseleave(){
      //this.el.nativeElement.style.background = ''
      this.color = ''
  }

  color='';

  //@HostBinding('style.background') 
  get getColor(){
    return this.color
  }

  ngOnInit() {
      //this.el.nativeElement.style.background = this.highlightColor;
      //this.renderer.setElementStyle(this.el.nativeElement,'background',this.highlightColor);
  }

  //@Attribute('highlight') highlight
  constructor(private el:ElementRef, private renderer:Renderer) {

  }

}
