import { Component, Input,
  OnInit, 
  OnChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy
 } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'lifecycle',
  template: `
    <p>
      lifecycle Works!
      <ng-content></ng-content>
    </p>
  `,
  styles: []
})
export class LifecycleComponent implements OnInit, 
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy
 {

  @Input() value:string = '';

  constructor() { 
    console.log('constructor',arguments);
  }

  ngOnInit() {
    console.log('ngOnInit',arguments);
  }

  ngOnChanges(changes){
    console.log('ngOnChanges',changes);
    for(let prop in changes){
      console.log('changed to',changes[prop].currentValue)
    }
  }

  ngDoCheck(){
    console.log('ngDoCheck',arguments);
  }
  
  ngAfterContentInit(){
    console.log('ngAfterContentInit',arguments);
  }
  
  ngAfterContentChecked(){
    console.log('ngAfterContentChecked',arguments);
  }
  
  ngAfterViewInit(){
    console.log('ngAfterViewInit',arguments);
  }
  
  ngAfterViewChecked(){
    console.log('ngAfterViewChecked',arguments);
  }
  
  ngOnDestroy(){
    console.log('ngOnDestroy',arguments);
  }
  


}
