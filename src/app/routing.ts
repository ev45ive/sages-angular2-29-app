import { Routes, RouterModule } from '@angular/router'
import { TodoAppComponent, 
    SearchComponent,
    BookmarksComponent, 
    HomeComponent,
    BookmarkComponent } from './index';

const routes:Routes = [
    { path:'todos', component: TodoAppComponent },
    { path:'search', component: SearchComponent },
    { path:'bookmarks', component: BookmarksComponent,
        children: [
            { path:'', component: BookmarkComponent },
            { path:':id', component: BookmarkComponent },
        ]
    },
    { path:'**', component: HomeComponent }
]

export const routingProviders = [

]

export const routing = RouterModule.forRoot(routes)