import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[Unless]'
})
export class UnlessDirective {

  constructor(private template: TemplateRef<any>,
              private viewContainer: ViewContainerRef) {
          
  }

  @Input() set Unless(visible: boolean){

    if(visible){
        this.viewContainer.clear();
        this.viewContainer.createEmbeddedView(this.template);
    }else{
        this.viewContainer.clear();
    }

  };


}
