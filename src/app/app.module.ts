import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing, routingProviders } from './routing'
import { 
  AppComponent, 
  TodoAppComponent, 
  SearchComponent,
  BookmarksComponent,
  NavComponent,
  BookmarkComponent, } from './index';
import { HomeComponent } from './home/home.component'
import { MusicService } from './search/music.service'
import { BookmarksService } from './search/bookmarks/bookmarks.service';
import { UnlessDirective } from './unless.directive';



@NgModule({
  // Views: Component, Directive, etc..
  declarations: [
    NavComponent,
    AppComponent,
    TodoAppComponent,
    SearchComponent,
    HomeComponent,
    BookmarksComponent,
    NavComponent,
    BookmarkComponent,
    UnlessDirective
  ],
  // Modules: Forms, Http, Routing...
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing
  ],
  // Service Providers
  providers: [
    routingProviders,
    MusicService,
    BookmarksService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
