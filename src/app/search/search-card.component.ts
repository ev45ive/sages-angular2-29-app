import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'search-card',
  template: `<div class="card">
      <div class="card-content">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: []
})
export class SearchCardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
