import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Album, AlbumImage } from './interfaces';
import { ShortenPipe } from '../shorten.pipe';

@Component({
  selector: 'album-card',
  template: `
      <img src="{{image.url}}"
          width={{image.width}} />
      <h3 class="title is-6">{{album.name|shorten:20}}</h3>
  `,
  styles: [],
  pipes: [ ShortenPipe ]
})
export class AlbumComponent implements OnInit, OnChanges {

  constructor() { }

  @Input() album: Album;

  image:AlbumImage;

  ngOnInit() {
  }
  ngOnChanges(){
    this.image = this.album.images[0];
  }

}
