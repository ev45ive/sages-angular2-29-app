export interface Album{
  id:string,
  name:string,
  images:AlbumImage[],
}
export interface AlbumImage{
  url:string,
  height:number,
  width: number
}

export interface Bookmark{
  id:number,
  name:string,
  album_id:string,
  comment?:string,
  rating?:number
}

export interface Card<T>{
    content:T
}
