import { Component, OnInit, Inject } from '@angular/core';
import { BookmarksService } from './bookmarks.service'


@Component({
  selector: 'bookmarks',
  template: `
    <h3 class="title">Bookmarks</h3>
    <div class="columns">
      <div class="column is-4">
        <ul class="">
          <li *ngFor="let card of cards " class="column is-fullwidth">
            <search-card [routerLink]="['/bookmarks', card.id ]"  routerLinkActive="is-active">
              {{card.name}}
            </search-card>
          </li>
        </ul>
      </div>
      <div class="column is-8">
        <router-outlet></router-outlet>
      </div>
    </div>
  `,
  styles: []
})
export class BookmarksComponent implements OnInit {

  constructor(private bookmarks: BookmarksService) {
    console.log('bookmarks');
    //snapshot
    this.cards = this.bookmarks.bookmarks;
    
    // updates
    this.bookmarks.bookmarksStream.subscribe(bookmarks=>{
      this.cards = bookmarks;
    })
    
    /*bookmarks.bookmarksStream.subscribe(items=>{
      this.cards = items;
    })*/
  };

  cards;

  ngOnInit(){
    this.bookmarks.getStream()
  }

}
