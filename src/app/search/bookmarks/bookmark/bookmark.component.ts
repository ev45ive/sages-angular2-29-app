import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookmarksService } from '../bookmarks.service'
import { Bookmark } from '../../interfaces';

@Component({
  selector: 'bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.css']
})
export class BookmarkComponent implements OnInit {

  constructor(private activeRoute:ActivatedRoute,
              private bookmarks:BookmarksService) {

    this.activeRoute.params.subscribe(params=>{
      let bookmark_id = parseInt(params['id']);
      if(bookmark_id){
        this.bookmark = this.bookmarks.getBookmark(bookmark_id);
      }
    })

  }

  bookmark:Bookmark;

  ngOnInit() {
    /*let bookmark_id = parseInt(this.activeRoute.snapshot.params['id']);
    if(bookmark_id){
      this.bookmark = this.bookmarks.getBookmark(bookmark_id);
    }*/
  }

}
