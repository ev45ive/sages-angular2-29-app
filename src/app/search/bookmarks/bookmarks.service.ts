import { Injectable, EventEmitter } from '@angular/core';
import { Bookmark } from '../interfaces';
import { Http, RequestOptions, Headers } from '@angular/http';
//import { Observable } from 'rxjs';


@Injectable()
export class BookmarksService {

  url = 'http://localhost:3000/bookmarks';

  constructor(private http:Http) {

   this.http.get(this.url)
    .map(response=>response.json())
    .subscribe((items)=>{
        this.bookmarks = items;
        this.bookmarksStream.emit(this.bookmarks)
    })

    
  }

  bookmarksStream:EventEmitter<Bookmark[]> = new EventEmitter<Bookmark[]>();

  bookmarks:Bookmark[] = [
  ]

  addBookmark(album){
    let newBookmark = {
      //id: Date.now(),
      name: album.name,
      album_id: album.id
    };

    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })

    this.http.post(this.url,newBookmark, options)
    .map(response=>response.json())
    .subscribe( bookmark =>{
      this.bookmarks.push(bookmark)
    });

  }

  removeBookmark(){

  }

  getBookmark(bookmark_id){
    return this.bookmarks.filter(({id})=>id==bookmark_id)[0];
  }

  getStream(){
    //var stream = Observable.from(this.bookmarks)

    this.bookmarksStream.emit(this.bookmarks)
  }

  getBookmarks(){
     return this.bookmarks;
  }

}
