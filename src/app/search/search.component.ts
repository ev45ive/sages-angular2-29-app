import { Component, OnInit, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { AlbumComponent } from './album.component';
import { SearchCardComponent } from './search-card.component';
import { Album, AlbumImage, Card } from './interfaces';
import { MusicService } from './music.service'
import { FormControl, FormGroup, FORM_DIRECTIVES, Validators, Validator,
   REACTIVE_FORM_DIRECTIVES, FormBuilder } from '@angular/forms';

import { BookmarksService } from './bookmarks/bookmarks.service'

import 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'search',
  templateUrl: 'search.component.html',
  styleUrls: ['search.component.css'],
  directives: [ AlbumComponent, SearchCardComponent ],
})
export class SearchComponent implements OnInit {

  constructor(private http:Http,
              private formBuilder:FormBuilder, 
              private store:MusicService,
              private bookmarks: BookmarksService) {
    this.searchForm = this.formBuilder.group({
      'query': ['',[
        Validators.minLength(3),
        Validators.required,
        this.customValidator('batman')
      ]]
    })
    this.searchForm.find('query').valueChanges
    .filter((_)=>_.length>3)
    .debounceTime(500)
    .distinctUntilChanged()
    .subscribe(status=>{
      console.log('value', status)
      this.search(status)
    })
    console.log(this.searchForm)
    
  }

  searchForm:FormGroup;

  isInvalid(fieldName){
    let field = this.searchForm.find(fieldName);
    return field.dirty && field.invalid;
  }

  customValidator = (notAllowed) => ((control:FormControl):Validator =>{
    return control.value === notAllowed? <any>{
      'custom': `No ${notAllowed} please`
    } : null;
  })

  onSubmit(){
    this.search(this.searchForm.value.query)
  }

  ngOnInit() {
      this.cards = Promise.resolve(this.convertToCard(
        this.store.loadLastQueryResults()
      ))
  }

  //cards:Card<Album>[] = [];
  //Promise<Array<Card<Album>>>
  cards;
  
  convertToCard(items){
    return items.map((item)=><Card<Album>>{content:item})
  }

  search(query){
   
   this.cards = this.store.search(query)
        .map(this.convertToCard)
        .map((cards:any)=>{
           return cards;
        })
  }

  addBookmark(album){
    this.bookmarks.addBookmark(album)
  }

}
