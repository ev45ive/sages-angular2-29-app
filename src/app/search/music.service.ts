import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Album, AlbumImage, Card } from './interfaces';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class MusicService {

  constructor(private http:Http) {

    console.log('MusicService')
  }

  request(url){
    return this.http.get(url)
      .map(response=>response.json())
  }

  albums:Observable<Album>;

  saveQueryResults(query){
    localStorage.setItem('query',JSON.stringify(query))
  }

  loadLastQueryResults(){
    return JSON.parse(localStorage.getItem('query'))
  }

  search(query){
    var url = `https://api.spotify.com/v1/search?q=${query}&type=album`

    return this.request(url)
    .map((data:any)=>{
      let results = data.albums.items;
      this.saveQueryResults(results);
      return results;      
    });
  }

}
